package erris.tesandroiddebus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Informasi extends AppCompatActivity {

    TextView lblInfo;
    Button btnBack;
    String info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_informasi);

        lblInfo = (TextView) findViewById(R.id.lblInfoContent);
        btnBack = (Button) findViewById(R.id.btnInfoBack);

        info = "Aplikasi ini adalah aplikasi yang dibuat dengan mengambil referensi desain dari aplikasi " +
                "Youtube Android. Aplikasi ini menampilkan sebuah list yang berisi galeri gambar.\n" +
                "Anda dapat me-refresh halaman galeri dengan menarik scroll ke bawah pada layout galeri, " +
                "dimana setiap di refresh aplikasi akan menambahkan 3 buah galeri baru." +
                "\n\n *) Anda dapat mengatur banyaknya galeri yang ditambahkan pada menu pengaturan." ;

        lblInfo.setText(info);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
