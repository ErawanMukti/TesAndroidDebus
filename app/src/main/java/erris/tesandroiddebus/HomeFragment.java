package erris.tesandroiddebus;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import erris.tesandroiddebus.others.ListGaleri;
import erris.tesandroiddebus.others.RecGaleriAdapter;

public class HomeFragment extends Fragment {

    Context c;
    SharedPreferences prefs;
    SharedPreferences.Editor prefsEdit;
    SwipeRefreshLayout swMain;
    RecyclerView rv;
    FloatingActionButton fabHelp;
    RecGaleriAdapter adapter;
    ArrayList<ListGaleri> list_galeri;
    String[] arr_judul;
    TypedArray arr_gambar;
    int qty, total;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_home_fragment, container, false);

        c          = getActivity().getBaseContext();
        prefs      = getActivity().getSharedPreferences("app", Context.MODE_PRIVATE);
        prefsEdit  = prefs.edit();
        swMain     = (SwipeRefreshLayout) view.findViewById(R.id.swRefreshUtama);
        rv         = (RecyclerView) view.findViewById(R.id.rvUtama);
        fabHelp    = (FloatingActionButton) getActivity().findViewById(R.id.fabHelp);
        arr_judul  = getResources().getStringArray(R.array.judul_artikel);
        arr_gambar = getResources().obtainTypedArray(R.array.gambar_artikel);

        loadData();

        rv.setHasFixedSize(true);
        RecyclerView.LayoutManager lm = new LinearLayoutManager(c, LinearLayoutManager.VERTICAL, false);
        rv.setLayoutManager(lm);

        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if ( dy > 0 ) {
                    fabHelp.hide();
                } else {
                    fabHelp.show();
                }
            }
        });

        swMain.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
                swMain.setRefreshing(false);
            }
        });
        return view;
    }

    private void loadData() {
        qty    = prefs.getInt("qty", 3);
        total  = prefs.getInt("total", 0);
        total += qty;

        list_galeri = new ArrayList<>();
        for ( int i=1; i<=total; i++ ) {
            int idx = i % 9;

            ListGaleri list = new ListGaleri();
            list.setGambar(arr_gambar.getResourceId(idx, 0));
            list.setJudul(arr_judul[idx]);
            list_galeri.add(0, list);
        }

        adapter = new RecGaleriAdapter(c, list_galeri);
        rv.setAdapter(adapter);

        prefsEdit.putInt("total", total);
        prefsEdit.commit();
    }

}
