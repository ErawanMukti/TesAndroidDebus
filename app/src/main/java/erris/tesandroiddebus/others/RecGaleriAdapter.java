package erris.tesandroiddebus.others;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import erris.tesandroiddebus.R;

public class RecGaleriAdapter extends RecyclerView.Adapter<RecGaleriAdapter.ViewHolder>  {
    private Context context;
    private ArrayList<ListGaleri> data;

    public RecGaleriAdapter(Context c, ArrayList<ListGaleri> data) {
        this.context = c;
        this.data = data;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Picasso.with(context).load(data.get(position).getGambar()).resize(800, 450).into(holder.imgRowThumb);
        holder.lblRowTitle.setText(data.get(position).getJudul());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lblRowTitle;
        private ImageView imgRowThumb;

        public ViewHolder(View itemView) {
            super(itemView);

            lblRowTitle = (TextView) itemView.findViewById(R.id.lblGaleriTitle);
            imgRowThumb = (ImageView) itemView.findViewById(R.id.imgGaleriThumb);
        }
    }

}
