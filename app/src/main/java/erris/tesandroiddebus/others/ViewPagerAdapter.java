package erris.tesandroiddebus.others;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import erris.tesandroiddebus.HomeFragment;
import erris.tesandroiddebus.UserFragment;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new HomeFragment();

            case 1:
                return new UserFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
}
