package erris.tesandroiddebus;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

public class UserFragment extends Fragment implements View.OnClickListener {

    ImageView imgLinkedin, imgFacebook, imgSkype;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_user_fragment, container, false);

        imgLinkedin = (ImageView) view.findViewById(R.id.imgLinkedin);
        imgFacebook = (ImageView) view.findViewById(R.id.imgFacebook);
        imgSkype    = (ImageView) view.findViewById(R.id.imgSkype);

        imgLinkedin.setOnClickListener(this);
        imgFacebook.setOnClickListener(this);
        imgSkype.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        String url = "";

        switch (id) {
            case R.id.imgLinkedin:
                url = "https://linkedin.com/in/erawan-mukti-10763546";
                break;

            case R.id.imgFacebook:
                url = "https://facebook.com/erawan.moekti";
                break;

            case R.id.imgSkype:
                url = "skype:profile_name?erawan.moekti";
                break;
        }

        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_VIEW);
        intent.addCategory(Intent.CATEGORY_BROWSABLE);
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }
}
