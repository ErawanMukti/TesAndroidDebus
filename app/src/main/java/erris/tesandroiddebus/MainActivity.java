package erris.tesandroiddebus;

import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;

import erris.tesandroiddebus.others.RecGaleriAdapter;
import erris.tesandroiddebus.others.ViewPagerAdapter;
import erris.tesandroiddebus.others.ListGaleri;

public class MainActivity extends AppCompatActivity {

    Context c;
    SharedPreferences prefs;
    SharedPreferences.Editor prefsEdit;
    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager vwUtama;
    FloatingActionButton fabHelp;
    RecGaleriAdapter adapter;
    ViewPagerAdapter fAdapter;
    String[] arr_setting = {"1", "3", "5"};
    int qty, total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        c          = this;
        prefs      = getSharedPreferences("app", Context.MODE_PRIVATE);
        prefsEdit  = prefs.edit();
        toolbar    = (Toolbar) findViewById(R.id.TbUtama);
        tabLayout  = (TabLayout) findViewById(R.id.TabUtama);
        vwUtama    = (ViewPager) findViewById(R.id.vwUtama);
        fabHelp    = (FloatingActionButton) findViewById(R.id.fabHelp);
        total      = prefs.getInt("total", 0);

        setSupportActionBar(toolbar);
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int pos = tab.getPosition();
                vwUtama.setCurrentItem(pos);

                if ( pos == 0 ){
                    fabHelp.show();
                } else {
                    fabHelp.hide();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        fAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        vwUtama.setAdapter(fAdapter);
        vwUtama.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                TabLayout.Tab tab = tabLayout.getTabAt(position);
                tab.select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        fabHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inInfo = new Intent(c, Informasi.class);
                startActivity(inInfo);
            }
        });

        if ( total < 3 ) {
            fabHelp.performClick();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_utama, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_setting:
                AlertDialog dialogSetting = new AlertDialog.Builder(c)
                        .setTitle("Jumlah Penambahan Data").setSingleChoiceItems(arr_setting, Arrays.asList(arr_setting).indexOf(String.valueOf(qty)), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                qty = Integer.valueOf(arr_setting[which]);
                            }
                        })
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                prefsEdit.putInt("qty", qty);
                                prefsEdit.commit();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialogSetting.show();
                break;

            case R.id.action_reset:
                AlertDialog dialogReset = new AlertDialog.Builder(c)
                        .setTitle("Reset Data")
                        .setMessage("Anda yakin akan melakukan reset data?")
                        .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                prefsEdit.putInt("qty", 3);
                                prefsEdit.putInt("total", 0);
                                prefsEdit.commit();

                                finish();
                                startActivity(getIntent());
                            }
                        })
                        .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).create();
                dialogReset.show();
                break;

            case R.id.action_exit:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

}
